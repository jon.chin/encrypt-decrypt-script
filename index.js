var fs = require('fs');
var ursa = require('ursa');
var crypto = require('crypto');
var generator = require('generate-password');
const crypto_algo = 'aes-256-ctr';
const key_separator = '||-- end of key --||';

function encrypt(key_filename, input_filename, file_type = 'binary', output_filename){
    // if no output_filename specified, assume to encrypt in place
    if(typeof output_filename == 'undefined'){
	output_filename = input_filename;
    }
    
    let crt = ursa.createPublicKey(fs.readFileSync(key_filename));
    let file_data = fs.readFileSync(input_filename, file_type);

    // generate a random password
    let password = generator.generate({
	length: 100,
	numbers: true
    });

    
    let cipher = crypto.createCipher(crypto_algo, password);
    let encrypted_password = crt.encrypt(password, 'utf8', 'base64');

    // todo: choose another password in miniscule chance that encrypted password contains key_separator
    
    let encrypted_payload = cipher.update(file_data, file_type, 'base64');
    encrypted_payload += cipher.final('base64');
    fs.writeFileSync(output_filename,
		     encrypted_password +
		     key_separator +
		     encrypted_payload);
}

function decrypt(key_filename, input_filename, output_filename){
    // if no output_filename specified, assume to encrypt in place
    if(typeof output_filename == 'undefined'){
	output_filename = input_filename;
    }
    let key = ursa.createPrivateKey(fs.readFileSync(key_filename));
    let raw_file_data = fs.readFileSync(input_filename, 'utf8');

    // separate encrypted password from encrypted payload
    let file_parts = raw_file_data.split(key_separator);
    let encrypted_password = file_parts.shift();
    
    // reassemble remaining parts on the very miniscule chance encrypted payload has key_separator
    let encrypted_payload = file_parts.join();

    let decrypted_password = key.decrypt(encrypted_password, 'base64', 'utf8');

    let decipher = crypto.createDecipher(crypto_algo, decrypted_password);

    let decrypted_payload = decipher.update(encrypted_payload, 'base64', 'base64');
    decrypted_payload += decipher.final('base64');
    fs.writeFileSync(output_filename, Buffer.from(decrypted_payload, 'base64'));
}

module.exports.encrypt = encrypt;
module.exports.decrypt = decrypt;
