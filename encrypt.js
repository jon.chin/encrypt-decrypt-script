var encdec = require('./index.js');
var prompt = require('prompt');

console.log('staRt encryption script');

prompt.start();
prompt.get(['public key filename', 'input filename', 'output filename'], function (err, argv){
    encdec.encrypt(
	argv['public key filename'],
	argv['input filename'],
	"binary",
	argv['output filename']
    );
});
