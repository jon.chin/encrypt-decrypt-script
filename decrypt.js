var encdec = require('./index.js');
var prompt = require('prompt');

console.log('staRt decryption script');

prompt.start();
prompt.get(['private key filename', 'input filename', 'output filename'], function (err, argv){
    encdec.decrypt(
	argv['private key filename'],
	argv['input filename'],
	argv['output filename']
    );
});
